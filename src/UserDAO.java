import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

public class UserDAO {

    private Session session;

    public UserDAO(Session session) {
        this.session = session;
    }

    public Integer create(User user) {
        Transaction transaction = session.beginTransaction();
        int id = (Integer) session.save(user);
        transaction.commit();
        return id;
    }

    public User read(Integer id) {
        User user = (User) session.get(User.class, id);
        return user;
    }

    public List<User> readAll() {
        return new ArrayList<>();
    }

    public void update(User user) {
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(user);
        transaction.commit();
    }

    public User delete(Integer id) {
        Transaction transaction = session.beginTransaction();
        User found = (User) session.get(User.class, id);
        if ( found != null )
            session.delete(found);
        transaction.commit();
        return found;
    }
}
