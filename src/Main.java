import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private final static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        UserDAO userDAO = new UserDAO(HibernateUtil.getInstance());

        User user1 = new User("first", "premier", "awal");
        User user2 = new User("second", "deuxiemme", "tani");
        User user3 = new User("third", "troisieme", "talit");

        userDAO.create(user1);
        userDAO.create(user2);
        userDAO.create(user3);

        User found = userDAO.read(1);
        logger.debug(found.toString());

        User deleted = userDAO.delete(3);
        logger.debug(found.toString());

        HibernateUtil.destroy();
    }
}
